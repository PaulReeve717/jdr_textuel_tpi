﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Reflection;
namespace JSON_Dungeon
{
    class Program
    {  
        static void Main(string[] args)
        {
            //Beginning of the game
            Console.WriteLine("Bienvenue dans JSON Dungeon!\nAppuyez sur n'importe quel touche pour commencer!\n");
            Console.ReadKey();
            GameLogic.Player = new Player();
            
            //Represent the event displayed at the prompt
            Event currentEvent = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("C1"));

            //Game loop
            while(true)
            {
                //Boolean that indicates if the player has done the event
                bool endOfEvent = false;
                
                //String that will be fielded with the name of the next JSON file to open
                string nextEventDesignation = "";

                //Event loop which repeat itself the player has done the event
                while(!endOfEvent)
                {
                    Console.WriteLine("\n-----------------------------------------");
                    Console.WriteLine(currentEvent.ToString());
                    Console.WriteLine("Actions universelles: a) Voir statistiques b) Voir/Gérer inventaire");
                    char playerInput = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n");
                    switch (playerInput)
                    {
                        //For every action, the prompt is repeated until the player accomplished or cancel the chosen action
                        //Display the player stat
                        case 'a':
                            Console.WriteLine(GameLogic.Player.ShowStats());
                            break;
                        //Display the inventory and allow the user to manage it
                        case 'b':
                            Console.WriteLine(GameLogic.Player.ShowInventory());
                            if(GameLogic.Player.Inventory.Count == 0)
                            {
                                break;
                            }
                            //Read what action the player want to do with his inventory
                            Console.WriteLine("\nQue voulez-vous faire: a) Equiper une arme/armure b) Utiliser un objet c) Jeter un objet r)Retour en arrière");
                            char inventoryAction = Console.ReadKey().KeyChar;
                            Console.WriteLine("\n");
                            switch(inventoryAction)
                            {
                                /* 
                                 * The prompt is repeated until the player accomplished or cancel the action.
                                 * The 'r' letter is used to cancel an action.
                                 */
                                //Equip Item
                                case 'a':
                                    while(true)
                                    {
                                        
                                        Console.WriteLine(GameLogic.Player.ShowInventory());
                                        Console.WriteLine("r) retour en arrière\nSélectionnez le numéro de l'arme/armure à équiper:");
                                        char index = Console.ReadKey().KeyChar;
                                        if (index == 'r')
                                        {
                                            break;
                                        }
                                        //The item index mustn't be outside of the inventory list
                                        if (GameLogic.CheckPlayerInput(index, GameLogic.Player.Inventory.Count))
                                        {
                                            try
                                            {
                                                GameLogic.Player.EquipItem(GameLogic.Player.Inventory[(int)Char.GetNumericValue(index) - 1]);
                                                Console.WriteLine(GameLogic.Player.ShowInventory());
                                                break;
                                            }
                                            catch(NotEquipableItem e)
                                            {
                                                Console.WriteLine(e.Message);
                                            }                             
                                            
                                        }
                                        else
                                        {
                                            Console.WriteLine("\nSélectionnez un numéro d'objet existant");
                                        }
                                    }
                                    
                                    break;
                                //Use Item
                                case 'b':
                                    while(true)
                                    {
                                        
                                        Console.WriteLine(GameLogic.Player.ShowInventory());
                                        Console.WriteLine("r) retour en arrière\nSélectionnez le numéro de l'objet à utiliser:");
                                        char index = Console.ReadKey().KeyChar;
                                        if (index == 'r')
                                        {
                                            break;
                                        }

                                        //The item index mustn't be outside of the inventory list
                                        if (GameLogic.CheckPlayerInput(index, GameLogic.Player.Inventory.Count) && (GameLogic.Player.Inventory[(int)Char.GetNumericValue(index) - 1].GetType().BaseType) == typeof(UsableItem)) 
                                        {
                                                GameLogic.Player.UseItem((UsableItem)GameLogic.Player.Inventory[(int)Char.GetNumericValue(index) - 1]);
                                                Console.WriteLine(GameLogic.Player.ShowStats());
                                                break;
                                            
                                        }
                                        else
                                        {
                                            Console.WriteLine("\nVeuillez sélectionner un objet utilisable (Potion)");
                                        }
                                    }
                                    break;
                                //Delete Item
                                case 'c':
                                    while(true)
                                    {
                                        Console.WriteLine(GameLogic.Player.ShowInventory());
                                        Console.WriteLine("r) Retour en arrière\nSélectionnez le numéro de l'objet à supprimer:");
                                        char index2 = Console.ReadKey().KeyChar;
                                        if(index2 == 'r')
                                        {
                                            break;
                                        }
                                        //The item index mustn't be outside of the inventory list
                                        if (GameLogic.CheckPlayerInput(index2, GameLogic.Player.Inventory.Count))
                                        {
                                            GameLogic.Player.DeleteItem(GameLogic.Player.Inventory[(int)Char.GetNumericValue(index2) - 1]);
                                            Console.WriteLine(GameLogic.Player.ShowInventory());
                                            break;
                                        }
                                        else
                                        {
                                            Console.WriteLine("\nSélectionnez un numéro d'objet existant");
                                        }
                                    }
                                    
                                    break;
                                default:
                                    //Reshow the event actions
                                    break;
                            }
                            break;
                        //Execute the event
                        default:
                            try
                            {
                               nextEventDesignation = currentEvent.Execute(playerInput);
                                //If the event goes right, the event is considered is considered to be done
                                endOfEvent = true;
                            }
                            //If the input is incorrect the event isn't considered to be done
                            catch (NotValidActionException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            break;
                    }
                    Console.WriteLine("\n\n\n");
                }
                currentEvent = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath(nextEventDesignation));

            }
            
            
            

        }
    }
}
