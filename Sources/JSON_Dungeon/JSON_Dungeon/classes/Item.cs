﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSON_Dungeon
{
    public class Item
    {
        public string Name;
        public int Bonus;
      
        public override string ToString()
        {
            return $"{Name} +{Bonus}";
        }
    }

    public class Weapon : Item
    {
        public override string ToString()
        {
            return base.ToString()+" ATT";
        }
    }

    public class Armor : Item
    {
        public override string ToString()
        {
            return base.ToString() + " DEF";
        }
    }

    public abstract class UsableItem : Item
    {
        public abstract void Use(Player player);
        
    }

    public class Potion : UsableItem
    {
        public BonusType BonusType;

        public override string ToString()
        {
            return base.ToString() + $" {BonusType}";
        }

        public override void Use(Player player)
        {
            switch (BonusType)
            {
                case BonusType.HP:
                    player.HP += Bonus;
                    break;
                case BonusType.MP:
                    player.MP += Bonus;
                    break;
                case BonusType.Strength:
                    player.Strength += Bonus;
                    break;
                case BonusType.Defence:
                    player.Defence += Bonus;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            player.DeleteItem(this);
            
        }
    }

    //Differents stats that a potion can rise
    public enum BonusType
    {
        HP,
        MP,
        Strength,
        Defence
    }

    public class Spell : UsableItem
    {
        public override void Use(Player player)
        {
            player.MP--;
        }
    }

}
