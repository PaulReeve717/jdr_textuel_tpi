﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSON_Dungeon
{
    public class NotValidActionException : Exception
    {
        private string errorMessage;
        public NotValidActionException()
        {
            errorMessage = "\nVous action n'est pas valide. Assurez-vous de: \n-Avoir rentré un seul caractère\n" +
                "-Ne pas avoir mis d'espace\n" +
                "-Avoir choisi une action correcte parmi celles disponibles";

        }

        public override string Message
        {
            get
            {
                return errorMessage;
            }

        }
    }

    public class NotEquipableItem : Exception
    {
        private string errorMessage;

        public NotEquipableItem()
        {
             this.errorMessage = "\nChoisissez un type d'objet équipable (Arme ou armure).";
        }
                        
        public override string Message
        {
            get
            {
                return errorMessage;
            }

        }
    }


    public class FullInventoryException : Exception
    {

    }

    public class ItemNotFoundException : Exception
    {

    }

    public class  NonUsableItemException : Exception
    {
        private string errorMessage;

        public NonUsableItemException()
        {
            this.errorMessage = "\nChoisissez un type d'objet utilisable (Potion).";
        }

        public override string Message
        {
            get
            {
                return errorMessage;
            }

        }
    }
}
