﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace JSON_Dungeon
{
    public class Creature
    {
        public int HP;
        public int Strength;
        public int Defence;

        //Method that represents a battle round. Returns true if the attack hit and false if not
        public bool Attack(Creature defender, int successTouchRate)
        {
            Random random = new Random();
            int diceRoll = random.Next(1, 7);
            if(diceRoll >= successTouchRate)
            {
                defender.HP--;
                return true;
            }
            else
            {
                return false;
            }
        }

        //Compare the attacker's attack and the defender's defence
        public int CalculateTouchRate(Creature defender, int attackBonus = 0, int defenceBonus = 0)
        {
            int attackValue = this.Strength + attackBonus;
            int defenceValue = defender.Defence + defenceBonus;
            int successTouchRate = 0;
            if (attackValue >= (2 * defenceValue))
            {
                successTouchRate = 2;
            }
            else if ((2 * attackValue) <= defenceValue)
            {
                successTouchRate = 6;
            }
            else if (attackValue == defenceValue)
            {
                successTouchRate = 4;
            }
            else if(attackValue > defenceValue)
            {
                successTouchRate = 3;
            }
            else if(attackValue < defenceValue)
            {
                successTouchRate = 5;
            }
            return successTouchRate;
        }
    }

    public class Player : Creature
    {
        #region constants
        //Constants that set the basic stats of the player
        private const int START_HP = 3;
        private const int START_STRENGTH = 3;
        private const int START_DEFENCE = 3;
        private const int START_MP = 2;
        private const int MAX_ITEMS = 4;
        #endregion constants
        #region attributes & constructor

        private int mp;
        private List<Item> inventory;
        private Weapon equippedWeapon;
        private Armor equippedArmor;
        

        public Player() : base()
        {
            HP = START_HP;
            Strength = START_STRENGTH;
            Defence = START_DEFENCE;
            mp = START_MP;
            inventory = new List<Item>();
            equippedWeapon = null;
            equippedArmor = null;
        }
        #endregion attributes & constructor

        #region getters
        public List<Item> Inventory
        {
            get
            {
                return inventory;
            }
            set
            {
                inventory = value;
            }
        }

        public Weapon EquippedWeapon
        {
            get
            {
                return equippedWeapon;
            }
        }

        public Armor EquippedArmor
        {
            get
            {
                return equippedArmor;
            }
        }

        public int MP
        {
            get
            {
                return mp;
            }
            set
            {
                mp = value;
            }
        }
        
        #endregion getters

        #region methods
        public string ShowStats()
        {
            return $"\nPV: {HP} / PM: {mp} / Force: {Strength} / Défense: {Defence}";
        }

        public string ShowInventory()
        {
            string inventoryView = "";
            if (inventory.Count == 0)
            {
                inventoryView = "\nVotre inventaire est vide";
            }
          
            int index = 1;
            foreach(Item item in inventory)
            {
                inventoryView += $"\n{index}: {item}";
                index++;
            }

            if(equippedWeapon != null)
            {
                inventoryView += $"\nArme équipée: {equippedWeapon}";
            }

            if (equippedWeapon != null)
            {
                inventoryView += $"\nArmure équipée: {equippedArmor}";
            }
            return inventoryView;
        }

        public void AddItem(Item item)
        {
            if(inventory.Count > MAX_ITEMS)
            {
                throw new FullInventoryException();
            }
            inventory.Add(item);
        }

        public void EquipItem(Item item)
        {
            
            if (item.GetType() == typeof(Weapon))
            {      
                equippedWeapon = (Weapon)item;
            }
            else if ((item.GetType() == typeof(Armor)))
            {                      
                equippedArmor = (Armor)item;
            }
            else
            {
                throw new NotEquipableItem();
            }

        }

        public void DeleteItem(Item item)
        {
            if(!inventory.Contains(item))
            {
                throw new ItemNotFoundException();
            }

            //If the deleted item is equipped, it's desequipped
            if(item == equippedArmor)
            {
                equippedArmor = null;
            }

            if(item == equippedWeapon)
            {
                equippedWeapon = null;
            }

            //The item is removed from the inventory
            inventory.Remove(item);
        }

        public void UseItem(UsableItem item)
        {

            if (item.GetType().BaseType != typeof(UsableItem))
            {
                throw new NonUsableItemException();
            }
            item.Use(this);
        } 
        #endregion methods
    }

    public class Monster : Creature
    {
    }

}
