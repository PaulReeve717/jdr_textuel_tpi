﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
namespace JSON_Dungeon
{
    public abstract class Event
    {
        public string Name;
        public string Fluff;


        public abstract string Execute(char playerInput);
        
        
        

        public override string ToString()
        {
            return $"{Name}\n\n {Fluff}\n\n";
        }
    }

    public class ChoiceEvent : Event
    {
        public List<ChoiceAction> Actions;
        public override string Execute(char playerInput)
        {
            //the char is converted to the index of the action chosen by the player
            int playerChoice = (int)Char.GetNumericValue(playerInput);
            if(playerChoice == -1)
            {
                throw new NotValidActionException();
            }
            playerChoice--;
            //The choice of the player is in the range of possible actions
            if(playerChoice < 0 || playerChoice > Actions.Count-1)
            {
                throw new NotValidActionException();
            }

            ChoiceAction chosenAction = Actions[playerChoice];
            return chosenAction.NextEvent;
        }

        public override string ToString()
        {

            string actionList = "Choix possibles:\n-----------------------------------------\n";
            int actionNumber = 1;
            foreach(Action action in this.Actions)
            {
                actionList += $"{actionNumber.ToString()}) {action.Text} \n";
                actionNumber++;
            }
            return base.ToString() + actionList;
        }
    }

    public class OneExitEvent : Event
    {
        public string NextEvent;

        public override string Execute(char playerInput)
        {
            return NextEvent;
        }

        
    }

    public class BattleEvent : OneExitEvent
    {
        public Monster Monster;

        public override string Execute(char playerInput)
        {
           Fight();
           return base.Execute(playerInput);
        }

        public void Fight()
        {
            Console.WriteLine("\nCombat\n--------------------------\n");
            //The fight is pursued until one of the two belligerants die
            while (Monster.HP > 0 && GameLogic.Player.HP > 0)
            {
                
                //Player attack
                Console.WriteLine($"Votre force: {GameLogic.Player.Strength} (+ {(GameLogic.Player.EquippedWeapon == null ? "0" : GameLogic.Player.EquippedWeapon.Bonus.ToString())}) / Défense du monstre: {Monster.Defence}\n Vous attaquez:\n");
                int attackBonus = 0;
                if(GameLogic.Player.EquippedWeapon != null)
                {
                    attackBonus = GameLogic.Player.EquippedWeapon.Bonus;
                }
                if (GameLogic.Player.Attack(Monster, GameLogic.Player.CalculateTouchRate(Monster, attackBonus: attackBonus)))
                {
                    
                    Console.WriteLine("Touché!\n");
                }
                else
                {
                    Console.WriteLine("Raté...\n");
                }
                
                //If the Monster is killed, we exit the while loop
                if (Monster.HP == 0)
                {
                    Console.WriteLine($"Vous avez battu le monstre!");
                    break;

                }

                Console.WriteLine($"PV restants du monstre: {Monster.HP}\n");

                //Monster attack
                Console.WriteLine($"Votre défense: {GameLogic.Player.Defence} (+ {(GameLogic.Player.EquippedArmor == null? "0" : GameLogic.Player.EquippedArmor.Bonus.ToString())}) / Force du monstre: {Monster.Strength}\n");

                Console.WriteLine("Le monstre attaque:\n");
                int defenceBonus = 0;
                if(GameLogic.Player.EquippedArmor != null)
                {
                    defenceBonus = GameLogic.Player.EquippedArmor.Bonus;
                }
                if (Monster.Attack(GameLogic.Player, Monster.CalculateTouchRate(GameLogic.Player, defenceBonus: defenceBonus)))
                {
                    Console.WriteLine("Touché...\n");
                }
                else
                {
                    Console.WriteLine("Raté!\n");
                }

                Console.WriteLine($"Vos PVs: {GameLogic.Player.HP}\n");
                //If the Player is killed, we exit the while loop
                if (GameLogic.Player.HP == 0)
                {
                    Console.WriteLine($"Vous êtes terrassé par le monstre!\n");
                    break;

                }

                
            }

            
            if(GameLogic.Player.HP == 0)
            {
                Console.WriteLine($"Fin de la partie");
                return;
            }
            

        }
    }

    public class LootEvent : OneExitEvent
    {
        public Weapon Weapon;
        public Armor Armor;
        public Potion Potion;
        public Spell Spell;
        public List<LootAction> Actions;

        public override string Execute(char playerInput)
        {
            //the char is converted to the index of the action chosen by the player
            int playerChoice = (int)Char.GetNumericValue(playerInput);
            if (playerChoice == -1.0)
            {
                throw new NotValidActionException();
            }
            playerChoice--;
            //The choice of the player is in the range of possible actions
            if (playerChoice < 0 || playerChoice > Actions.Count - 1)
            {
                throw new NotValidActionException();
            }

            //If the action contains the Item, the player gain it
            LootAction chosenAction = Actions[playerChoice];
            if (chosenAction.HasLoot)
            {
                //Look what type of Item has been instanciated by the event and add it to the Inventory of the player
                if(Weapon != null)
                {
                    GameLogic.Player.AddItem(this.Weapon);
                }
                else if(Armor != null)
                {
                    GameLogic.Player.AddItem(this.Armor);
                }
                else if(Potion != null)
                {
                    GameLogic.Player.AddItem(this.Potion);
                }
                else if(Spell != null)
                {
                    GameLogic.Player.AddItem(this.Spell);
                }
                
            }
            return base.Execute(playerInput);
        }

        public override string ToString()
        {

            string actionList = "Choix possibles:\n-----------------------------------------\n";
            int actionNumber = 1;
            foreach (Action action in this.Actions)
            {
                actionList += $"{actionNumber.ToString()}) {action.Text} \n";
                actionNumber++;
            }
            return base.ToString() + actionList;
        }
    }

    public class TransitionEvent : OneExitEvent
    {
        public override string Execute(char playerInput)
        {
            return base.Execute(playerInput);
        }
    }

    
    public static class GameLogic
    {
        public static Player Player;


        //Construct the path of the next event
        public static string GetNextEventPath(string eventDesignation)
        {
            //Get the Appdata path where are the Json files
            string pathToJsonFiles = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            return pathToJsonFiles + @"\JSON_Dungeon\JSONFiles\" + eventDesignation + ".json";
        }

        //Deserialize the event from the JsonFile to the right type of Event Object
        public static Event DeserializeEvent(string pathToEvent)
        {
            if(!File.Exists(pathToEvent))
            {
                throw new Exception(pathToEvent);
            }
           
            string json = File.ReadAllText(pathToEvent);
            string fileName = Path.GetFileName(pathToEvent);

            //First letter of the file name indicates its type
            char eventType = fileName[0];

            switch(eventType)
            {
                case 'C':
                    return JsonConvert.DeserializeObject<ChoiceEvent>(json);
                    break;
                case 'L':                    
                    return JsonConvert.DeserializeObject<LootEvent>(json);
                    break;
                case 'B':
                    return JsonConvert.DeserializeObject<BattleEvent>(json);
                    break;
                case 'T':
                    return JsonConvert.DeserializeObject<TransitionEvent>(json);
                    break;
                default:
                    return null;
                    break;

            }
            
        }

        //Basic check of the player input
        public static bool CheckPlayerInput(char playerInput, int listSize)
        {
            //Check if the player input is numeric and if it does not points outside of the list it try to access
            if (!Char.IsDigit(playerInput))
            {
                return false;
            }
            int playerNumber = (int)Char.GetNumericValue(playerInput);
           return (playerNumber > 0 && playerNumber <= listSize);
        }
        
    }
}
