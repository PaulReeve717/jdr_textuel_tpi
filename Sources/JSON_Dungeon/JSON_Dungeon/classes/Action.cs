﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSON_Dungeon
{
    public class Action
    {
        public string Text;
    }

    public class ChoiceAction : Action
    {
        public string NextEvent;
    }

    public class LootAction : Action
    {
        public bool HasLoot;
        
    }
}
