﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JSON_Dungeon;
namespace Test_JSON_Dungeon
{
    [TestClass]
    public class playerTest
    {
        private Player player;
        

        [TestInitialize]
        public void Initialize()
        {
            player = new Player();
            
        }

        [TestMethod]
        public void ShowBeginningStatsTest()
        {
            Assert.AreEqual("PV: 3 / PM: 2 / Force: 3 / Défense: 3", player.ShowStats());
        }

        [TestMethod]
        public void ShowChangedStatsTest()
        {
            player.HP--;
            player.Defence++;
            Assert.AreEqual("PV: 2 / PM: 2 / Force: 3 / Défense: 4", player.ShowStats());
        }
    }

    [TestClass]
    public class eventTest
    {
        private Player player;
        private Event event1;
        [TestMethod]
        public void DeserializeChoiceEventTest()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("C1"));
            Assert.AreEqual(typeof(ChoiceEvent), event1.GetType());
        }

        [TestMethod]
        public void DeserializeLootEventTest()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("L1"));
            Assert.AreEqual(typeof(LootEvent), event1.GetType());
        }

        [TestMethod]
        public void DeserializeBattleEventTest()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("B1"));
            Assert.AreEqual(typeof(BattleEvent), event1.GetType());
        }

        [TestMethod]
        public void DeserializeTransitionEventTest()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("T1"));
            Assert.AreEqual(typeof(TransitionEvent), event1.GetType());
        }

        [TestMethod]
        public void GoOnTheNextEventWithOneExitEventTest()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("T1"));
           string eventDesignation = event1.Execute('a');
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath(eventDesignation));
            Assert.AreEqual("Une question magique", event1.Name);
            
        }

        [TestMethod]
        public void GoOnTheNextEventWithChoiceEventTest1()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("C1"));
            string eventDesignation = event1.Execute('1');
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath(eventDesignation));
            Assert.AreEqual("Un premier objet", event1.Name);
        }

        [TestMethod]
        public void GoOnTheNextEventWithChoiceEventTest2()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("C1"));
            string eventDesignation = event1.Execute('2');
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath(eventDesignation));
            Assert.AreEqual("Un premier équipement", event1.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(NotValidActionException))]
        public void ChooseANotValidActionInEvent()
        {
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath("C1"));
            string eventDesignation = event1.Execute('3');
            event1 = GameLogic.DeserializeEvent(GameLogic.GetNextEventPath(eventDesignation));
            
        }


    }

    [TestClass]
    public class inventoryTest
    {
        private Player player = new Player();
        private Event event1;
        private Weapon weapon1 = new Weapon();
        private Weapon weapon2 = new Weapon();
        private Armor armor = new Armor();
        private Potion hpPotion = new Potion();
        private Potion mpPotion = new Potion();
        private Spell spell = new Spell();

        [TestInitialize]
        public void init()
        {
            weapon1.Name = "Epée en bois";
            weapon1.Bonus = 1;

            weapon2.Name = "Masse de pierre";
            weapon2.Bonus = 2;

            armor.Name = "Armure en bois";
            armor.Bonus = 1;

            hpPotion.Name = "Potion de soin";
            hpPotion.Bonus = 2;
            hpPotion.BonusType = BonusType.HP;

            mpPotion.Name = "Potion de magie";
            mpPotion.Bonus = 2;
            mpPotion.BonusType = BonusType.MP;

            spell.Name = "Wololo";
            spell.Bonus = 3;

        }

        [TestMethod]
        public void AddItemToInventoryTest()
        {
            player.AddItem(weapon1);
            Assert.AreEqual(1, player.Inventory.Count);
            Assert.AreEqual(typeof(Weapon), player.Inventory[0].GetType());
        }

        [TestMethod]
        public void AddMultipleItemsToInventoryTest()
        {
            player.AddItem(weapon1);
            player.AddItem(weapon2);    
            Assert.AreEqual(2, player.Inventory.Count);
            Assert.AreEqual("Epée en bois", player.Inventory[0].Name);
            Assert.AreEqual("Masse de pierre", player.Inventory[1].Name);
            
        }

        [TestMethod]
        public void DeleteInventoryItemTest()
        {
            player.AddItem(weapon1);
            player.AddItem(weapon2);
            player.DeleteItem(weapon1);
            Assert.AreEqual(weapon2, player.Inventory[0]);
        }

        [TestMethod]
        public void EquipWeaponTest()
        {
            player.AddItem(weapon1);
            player.EquipItem(weapon1);
            Assert.AreEqual("Epée en bois", player.EquippedWeapon.Name);
            
        }

        [TestMethod]
        public void EquipWeaponWhenAWeaponIsAlreadyEquippedTest()
        {
            player.AddItem(weapon1);
            player.AddItem(weapon2);
            player.EquipItem(weapon1);
            player.EquipItem(weapon2);
            Assert.AreEqual("Masse de pierre", player.EquippedWeapon.Name);
        }

        [TestMethod]
        public void UseHPPotion()
        {
            int initialHealth = player.HP;
            player.AddItem(hpPotion);
            Assert.AreEqual(1, player.Inventory.Count);
            player.UseItem(hpPotion);
            Assert.AreEqual(0, player.Inventory.Count);
            Assert.AreEqual(initialHealth+hpPotion.Bonus, player.HP);
            
        }

        [TestMethod]
        public void UseMPPotion()
        {
            int initialMagic = player.MP;
            player.AddItem(mpPotion);
            player.UseItem(mpPotion);
            Assert.AreEqual(initialMagic + mpPotion.Bonus, player.MP);
        }
    }
}
